#!/bin/bash
mkdir -p out
if [ ! -f "out/openssl.cnf" ] ; then
    cp openssl-template.cnf out/openssl.cnf
fi

defaultCountryName=$(grep -oE "countryName = .+\$" out/openssl.cnf)
defaultCountryName=${defaultCountryName#"countryName = "}
read -p "Country code (2 letters) [$defaultCountryName]: " countryName
countryName=${countryName:-"$defaultCountryName"}

defaultStateOrProvinceName=$(grep -oE "stateOrProvinceName = .+\$" out/openssl.cnf)
defaultStateOrProvinceName=${defaultStateOrProvinceName#"stateOrProvinceName = "}
read -p "State or province name [$defaultStateOrProvinceName]: " stateOrProvinceName
stateOrProvinceName=${stateOrProvinceName:-"$defaultStateOrProvinceName"}

defaultLocalityName=$(grep -oE "localityName = .+\$" out/openssl.cnf)
defaultLocalityName=${defaultLocalityName#"localityName = "}
read -p "Locality name (e.g. city) [$defaultLocalityName]: " localityName
localityName=${localityName:-"$defaultLocalityName"}

defaultOrganizationName=$(grep -oE "organizationName = .+\$" out/openssl.cnf)
defaultOrganizationName=${defaultOrganizationName#"organizationName = "}
read -p "Organization name [$defaultOrganizationName]: " organizationName
organizationName=${organizationName:-"$defaultOrganizationName"}

defaultOrganizationalUnitName=$(grep -oE "organizationalUnitName = .+\$" out/openssl.cnf)
defaultOrganizationalUnitName=${defaultOrganizationalUnitName#"organizationalUnitName = "}
read -p "Organizational unit name [$defaultOrganizationalUnitName]: " organizationalUnitName
organizationalUnitName=${organizationalUnitName:-"$defaultOrganizationalUnitName"}

defaultCommonName=$(grep -oE "commonName = .+\$" out/openssl.cnf)
defaultCommonName=${defaultCommonName#"commonName = "}
read -p "Common name (domain name) [$defaultCommonName]: " commonName
commonName=${commonName:-"$defaultCommonName"}

readResult="$commonName"

dns="DNS.1 = $commonName"

i=2
while ! [[ -z $readResult ]]
do
    read -p "DNS.$i [] (leave blank to skip adding more FQDN's): " readResult
    if ! [[ -z $readResult ]] ; then
        # Add newline after previous DNS entry
        dns="$dns
DNS.$i = $readResult"
        let "i+=1"
    fi
done

cat out/openssl.cnf |
    perl -s -0777 -pe 's/$from/$to/g' -- -from='countryName = .*' -to="countryName = $countryName" - |
    perl -s -0777 -pe 's/$from/$to/g' -- -from='stateOrProvinceName = .*' -to="stateOrProvinceName = $stateOrProvinceName" - |
    perl -s -0777 -pe 's/$from/$to/g' -- -from='localityName = .*' -to="localityName = $localityName" - |
    perl -s -0777 -pe 's/$from/$to/g' -- -from='organizationName = .*' -to="organizationName = $organizationName" - |
    perl -s -0777 -pe 's/$from/$to/g' -- -from='organizationalUnitName = .*' -to="organizationalUnitName = $organizationalUnitName" - |
    perl -s -0777 -pe 's/$from/$to/g' -- -from='commonName = .*' -to="commonName = $commonName" - |
    (
        perl -s -0777 -pe 's/$from/$to/g' -- -from='(DNS\.[0-9]+ = .*\n?)+' -to="$dns" - ;
        printf '\n'
    ) > out/openssl.cnf

defaultOutputPrefix=$(echo "$commonName" | sed 's/[^a-zA-Z0-9_\-\.]//g')

read -p "Output file prefix [$defaultOutputPrefix]: " outputPrefix

outputPrefix="${outputPrefix:-"$defaultOutputPrefix"}"

read -p "Configuration done. Press enter to generate CA and cert with keys."

cd out; openssl req -config openssl.cnf -new -out "$outputPrefix"_ca.csr -keyout "$outputPrefix"_ca.pem &&
    openssl rsa -in "$outputPrefix"_ca.pem -out "$outputPrefix"_ca.key &&
    openssl x509 -sha256 -in "$outputPrefix"_ca.csr -out "$outputPrefix"_ca.cert -req -signkey "$outputPrefix"_ca.key -days 365 &&
    openssl req -config openssl.cnf -new -out "$outputPrefix".csr -keyout "$outputPrefix".pem &&
    openssl rsa -in "$outputPrefix".pem -out "$outputPrefix".key &&
    openssl x509 -sha256 -in "$outputPrefix".csr -CA "$outputPrefix"_ca.cert -CAkey "$outputPrefix"_ca.pem -CAcreateserial -out "$outputPrefix".cert -req -days 365 -extensions req_ext -extfile openssl.cnf; cd ..
