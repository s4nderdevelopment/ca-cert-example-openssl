# Generating self-signed CA and CA-signed certificate

This repository contains scripts to automatically generate:
- A CA (certificate authority) certificate with private key
- A certificate with private key, signed by the CA

It outputs the generated files into the `out/` directory.

See [Usage for Windows users](#usage-for-windows-users) or [Usage for Linux users](#usage-for-linux-users) below.

Example use cases:

- You are running a local HTTPS website on your computer and you need a self signed certificate. To prevent HTTPS warnings, you can:
  - Trust the CA certificate in your browser
  - Add the website certificate and private key to your webserver
- You are running an HTTPS web server in a container. You also have a reverse proxy (Apache, Nginx) pointing to the container. You can:
  - Trust the certificate or CA certificate on the reverse proxy
  - Add the website certificate and private key to the webserver in the container

### Disclaimers and notes

*Disclaimer: I am not responsible if these scripts generate insecure certificates.*

*Note 1: If you encounter any insecure settings in the scripts or configuration file defaults, please check if the issue has been reported already by checking [Known issues](#known-issues). When you have found a new issue, feel free to report it in the GitLab issues tab on the side.*

*Note 2: For now, this is a hobby project. Don't expect bugs to be fixed quickly.*

*Note 3: You may also post feature requests by creating an issue. For now, this is a hobby project, so maybe I will add it in the future, maybe not. Or feel free to fork this project and add it yourself.*

### Usage for Windows users

No need to install WSL, Git Bash or MinGW/Cygwin/etc.

If you have WSL installed, follow the preferred method under [Usage for Linux users](#usage-for-linux-users).

Open powershell, change to the directory of this repository (after cloning, or downloading + unzipping).

Run `.\gencerts.ps1` to get started. Fill in the fields.

### Usage for Linux users

Requires `bash`, `perl` and `openssl` to be installed.

Works with:
- WSL (tested)
- Any Linux instance (tested)
- Git Bash (should work or might work, not tested)
- MinGW (should work or might work, not tested)
- Cygwin (should work or might work, not tested)

Run `./gencerts.sh` to get started. Fill in the fields. Output:

```
$ ./gencerts.sh
Country code (2 letters) [XX]: NL
State or province name [Your-State]: My-Province
Locality name (e.g. city) [Your-City]: My-City
Organization name [Your-Organization]: My-Organization
Organizational unit name [Your-Organization]: My-Organizational-Unit
Common name (domain name) [your-hostname]: localhost
NL
Configuration done. Press enter to generate CA and cert with keys.
Generating a RSA private key
............+++++
.+++++
writing new private key to 'localhost_ca.pem'
-----
writing RSA key
Signature ok
subject=C = NL, ST = My-Province, L = My-City, O = My-Organization, OU = My-Organizational-Unit, CN = localhost
Getting Private key
Generating a RSA private key
........................+++++
.+++++
writing new private key to 'localhost.pem'
-----
writing RSA key
Signature ok
subject=C = NL, ST = My-Province, L = My-City, O = My-Organization, OU = My-Organizational-Unit, CN = localhost
Getting CA Private Key
```

### Work in progress

To do list:

- [ ] Adding multiple `Subject Alternative Name` values with `DNS.$i`
  - [x] Bash script file
  - [ ] Powershell script file

### Known issues

List of known issues and considerations:
- Security
  - Issues
    - None for now
  - Considerations
    - When generating the certificates, code injection might be possible. Don't run the `./gencerts.sh` and `.\gencerts.ps1` scripts in an automated environment with unsanitized user input.
- Other
  - Issues
    - None for now
  - Considerations
    - The powershell script `.\gencerts.ps1` does not support multiple `Subject Alternative Name` values.
    - The PowerShell script for Windows users has not been thouroughly tested. Maybe you will run into errors.
