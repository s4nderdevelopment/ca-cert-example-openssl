New-Item -Type Directory -Force out | Out-Null

$confFile = "out\openssl.cnf"

if(-Not (Test-Path $confFile)){
    Copy-Item openssl-template.cnf -Destination out\openssl.cnf | Out-Null
}

$confFileContent = Get-Content $confFile

$defaultCountryName=($confFileContent | Select-String -Pattern "countryName = .+$" -CaseSensitive).Line
$countryName = Read-Host -Prompt "Country code (2 letters) [$defaultCountryName]"
if([string]::IsNullOrWhiteSpace($countryName)){
    $countryName = $defaultCountryName.SubString("countryName = ".Length)
}
$countryName = $countryName.Trim()

$defaultStateOrProvinceName=($confFileContent | Select-String -Pattern "stateOrProvinceName = .+$" -CaseSensitive).Line
$stateOrProvinceName = Read-Host -Prompt "State or province name [$defaultStateOrProvinceName]"
if([string]::IsNullOrWhiteSpace($stateOrProvinceName)){
    $stateOrProvinceName = $defaultStateOrProvinceName.SubString("stateOrProvinceName = ".Length)
}
$stateOrProvinceName = $stateOrProvinceName.Trim()

$defaultLocalityName=($confFileContent | Select-String -Pattern "localityName = .+$" -CaseSensitive).Line
$localityName = Read-Host -Prompt "Locality name (e.g. city) [$defaultLocalityName]"
if([string]::IsNullOrWhiteSpace($localityName)){
    $localityName = $defaultLocalityName.SubString("localityName = ".Length)
}
$localityName = $localityName.Trim()

$defaultOrganizationName=($confFileContent | Select-String -Pattern "organizationName = .+$" -CaseSensitive).Line
$organizationName = Read-Host -Prompt "Organization name [$defaultOrganizationName]"
if([string]::IsNullOrWhiteSpace($organizationName)){
    $organizationName = $defaultOrganizationName.SubString("organizationName = ".Length)
}
$organizationName = $organizationName.Trim()

$defaultOrganizationalUnitName=($confFileContent | Select-String -Pattern "organizationalUnitName = .+$" -CaseSensitive).Line
$organizationalUnitName = Read-Host -Prompt "Organizational unit name [$defaultOrganizationalUnitName]"
if([string]::IsNullOrWhiteSpace($organizationalUnitName)){
    $organizationalUnitName = $defaultOrganizationalUnitName.SubString("organizationalUnitName = ".Length)
}
$organizationalUnitName = $organizationalUnitName.Trim()

$defaultCommonName=($confFileContent | Select-String -Pattern "commonName = .+$" -CaseSensitive).Line
$commonName = Read-Host -Prompt "Common name (domain name) [$defaultCommonName]"
if([string]::IsNullOrWhiteSpace($commonName)){
    $commonName = $defaultCommonName.SubString("commonName = ".Length)
}
$commonName = $commonName.Trim()

$confFileContent = (($confFileContent + "") -Replace 'countryName = .*$', ("countryName = " + $countryName))
$confFileContent = (($confFileContent + "") -Replace 'stateOrProvinceName = .*$', ("stateOrProvinceName = " + $stateOrProvinceName))
$confFileContent = (($confFileContent + "") -Replace 'localityName = .*$', ("localityName = " + $localityName))
$confFileContent = (($confFileContent + "") -Replace 'organizationName = .*$', ("organizationName = " + $organizationName))
$confFileContent = (($confFileContent + "") -Replace 'organizationalUnitName = .*$', ("organizationalUnitName = " + $organizationalUnitName))
$confFileContent = (($confFileContent + "") -Replace 'commonName = .*$', ("commonName = " + $commonName))
$confFileContent = (($confFileContent + "") -Replace 'DNS\.1 = .*$', ("DNS.1 = " + $commonName))

$confFileContent = $confFileContent.Trim()

$confFileContent | Set-Content -Path $confFile

$defaultOutputPrefix = ($commonName -Replace '[^a-zA-Z0-9_\-\.]', "")
$outputPrefix = Read-Host -Prompt "Output file prefix [$defaultOutputPrefix]"
if([string]::IsNullOrWhiteSpace($outputPrefix)){
    $outputPrefix = $defaultOutputPrefix
}
$outputPrefix = $outputPrefix.Trim()

Read-Host -Prompt "Configuration done. Press enter to generate CA and cert with keys." | Out-Null

Set-Location -Path out
openssl req -config openssl.cnf -new -out "${outputPrefix}_ca.csr" -keyout "${outputPrefix}_ca.pem"
openssl rsa -in "${outputPrefix}_ca.pem" -out "${outputPrefix}_ca.key"
openssl x509 -sha256 -in "${outputPrefix}_ca.csr" -out "${outputPrefix}_ca.cert" -req -signkey "${outputPrefix}_ca.key" -days 365
openssl req -config openssl.cnf -new -out "$outputPrefix.csr" -keyout "$outputPrefix.pem"
openssl rsa -in "$outputPrefix.pem" -out "$outputPrefix.key"
openssl x509 -sha256 -in "$outputPrefix.csr" -CA "${outputPrefix}_ca.cert" -CAkey "${outputPrefix}_ca.pem" -CAcreateserial -out "$outputPrefix.cert" -req -days 365 -extensions req_ext -extfile openssl.cnf
Set-Location -Path ..
